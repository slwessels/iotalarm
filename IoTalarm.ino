#include <espAlarm.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <SimpleTimer.h>


// the timer object
SimpleTimer timer;

const char* ssid = "Wessels home";
const char* password = "tuisnetwerkM3lani3St3phanW3ss3ls";
const char* mqtt_server = "fullyautomatics";

//Temperature Defintions
#define REPORT_INTERVAL 60 // in sec
// #define ONE_WIRE_BUS 10  // DS18B20 pin
// OneWire oneWire(ONE_WIRE_BUS);
// DallasTemperature DS18B20(&oneWire);
float oldTemp;


//Define EspAlarm
EspAlarm iotAlarm = EspAlarm(&externalComms);

//Define Wifi and MQTT
WiFiClient espClient;
PubSubClient client(espClient);

WiFiClient wifiClient;

//-----------------------------------------------------------------------
//Setup
//-----------------------------------------------------------------------
//Call back function for IoTAlarm Comms via Serial and MQTT
void externalComms(String message)
{
  Serial.println(String(message));
  //Set debug info to serial port

  //Send to mqtt server also
  message = "IoTAlarm_"+String(WiFi.localIP())+": " +message;
  client.publish("Alarm/1/debug", message.c_str());
}

void runAlarm()
{
  iotAlarm.run();
}

void postUptime() {
    int duration = millis();
    int Milliseconds = (duration%1000)/100;
    int Hours = (duration/(1000*60*60))%24;
    int Minutes = (duration/(1000*60))%60;
    int Seconds = (duration/1000)%60;

    String msg = "Uptime: " + (String)Hours + ":" + (String)Minutes + ":" + (String)Seconds;
    client.publish("Alarm/1/uptime", msg.c_str());
}


void hwSetup()
{// put your setup code here, to run once:
  // start serial communication.
  Serial.begin(115200);
  delay(100);

  iotAlarm.setup();

  //Temp setup
  oldTemp = -1;

  //Setup Timer events
  // timer.setInterval(10000, readTemp);

  //Setup a timer to report Uptime to MQTT every minute
  timer.setInterval(60000, postUptime);

  //Setup timer for external Connectivity
  timer.setInterval(10000,checkComms);

  //Run alarm ever 500ms to check for changes
  timer.setInterval(500, runAlarm);

}

void setup_wifi() {
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  // delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  WiFi.begin(ssid, password);

  //For the initial connect lets try for 5 x 500ms
  for (int i=0; i<5; i++)
  {
   if ((WiFi.status() != WL_CONNECTED))
   {
    // delay(500);
    Serial.print(".");
    }
    else
    {
      Serial.println("");
      Serial.println("WiFi connected");
      Serial.println("IP address: ");
      Serial.println(WiFi.localIP());
      break; //No need to loop any further
    }
  }
}

void checkComms()
{
  //Verify that network is connected
  if (WiFi.status() != WL_CONNECTED)
  {
    reconnect_wifi();
  }
  //Verify that MQTT server is connected
  if (!client.connected())
  {
    reconnect();
  }
}

void reconnect_wifi() {
  WiFi.begin(ssid, password);
  // delay (100);

  //Only print status if successfully connected
  if (WiFi.status() == WL_CONNECTED)
  {
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  }
  else
  {
    Serial.println("WiFi connection FAILED");
  }
}

void reconnect() {
  // Loop until we're reconnected
  if (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("IoTAlarm_"+WiFi.localIP())) {
      Serial.println("connected");
      externalComms("- connected");
      // Once connected, publish an announcement...
      // client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe("Alarm/1/action");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again later");
      // Wait 5 seconds before retrying
      //  delay(1000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length)
{
  // Serial.print("Message arrived [");
  // Serial.print(topic);
  // Serial.print("] ");
  String msg = "";
  for (int i = 0; i < length; i++)
  {
    msg += (char)payload[i];
  }
  // Serial.println(msg);

  // Switch on the alarm if message received
  if (String(topic) == "Alarm/1/action")
  {
    externalComms("Message received on TOPIC: Alarm/1/action:" + String(msg));
    // msg == String("disarm") ? iotAlarm.setDisArmed(true) : iotAlarm.setDisArmed(false);

    if (msg == String("disarm"))
    {
      iotAlarm.setDisArmed(true);
    }
    else if (msg == String("arm"))
    {
      iotAlarm.setDisArmed(false);
    }
  }
}

void setup()
{
  Serial.print("Hello World!");
  //Do hardware initialisation
  Serial.print("Do hardware initialisation!");
  hwSetup();
  //Future also setup software - server comms etc
  Serial.print("setup software - server comms etc");
  setup_wifi();
}

// void readTemp()
// {
//   float temp;
//
//   do {
//     DS18B20.requestTemperatures();
//     temp = DS18B20.getTempCByIndex(0);
//     // Serial.print("Temperature: ");
//     // Serial.println(temp);
//   } while (temp == 85.0 || temp == (-127.0));
//
//   if (temp != oldTemp)
//   {
//     String tempStr = String(temp);
//     // externalComms("New Temperature: "+tempStr);
//     client.publish("Alarm/1/temp", tempStr.c_str());
//
//     oldTemp = temp;
//   }

  // int cnt = REPORT_INTERVAL;
  //
  // while(cnt--)
  //   delay(1000);

// }

void loop()
{
  //Should typically only fire this once every minute or so
  client.loop();
  timer.run();
}
