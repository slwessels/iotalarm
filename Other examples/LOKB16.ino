//LOKB16
//ver 14 Oct 2011

// See http://sheepdogguides.com/arduino/aht8lokb16.htm
//    for a guide to LokB16.. my Arduino driven burglar alarm

//Establish where inputs will be connected...
int8_t bArmedWantedPin=3;
int8_t bBurgSeenPin=4;
int8_t bFireSeenPin=5;

//Establish where outputs will be connected...
int8_t bBeepPin=12;//
//Yes... if both of the following set to the same
//  value, the noise made for a fire, and the noise
//  for a burglar will be the same... a Bad Idea...
//  But at least the software is written so that the
//  installer has a choice between doing the right
//  thing, and doing the inexpensive thing!
int8_t bBurglarBellPin=11;
int8_t bFireBellPin=10;

//The following five determine speed Morse code sent...
int16_t bDit=150;//
int16_t bDah=bDit*3;//
int16_t bPostBit=bDit;//
int16_t bPostLett=(bDit*3)-bPostBit;//
int16_t bPostWord=(bDit*7)-bPostBit;//
/*I read on the web that "proper" Morse uses...
    Each dit is one element
    Each dah is three elements
    IntRA-character spacing (what I've called "PostBit") is one element
    IntER-character spacing is three elements
    And inter-word spacing is seven elements.
    The "-bPostBit", in bPostLett and bPostWord is because
    after every dit or dah, a bPostBit delay
    occurs, on the assumption that another
    dit or dah may be coming. Yes... as long as bPostBit=bDit,
    the formulae could be simplified... but who knows how you
    want to configure your sending speeds?*/

boolean boBurglarBellOn=false;//added in patch of 15Oct11
boolean boFireBellOn=false;//added in patch of 15Oct11


//History: LOKB16 started Oct 11... after years of "meaning to..."

boolean boPrevArmed;

//****************************
boolean csWantArmed(){
if (digitalRead(bArmedWantedPin)==HIGH)
            return true; else return false;
}

//****************************
boolean csFireSeen()
/*"if...==LOW" is right if...
a) You use int internal pull up resistors on bFireSeenPin, and
b)The sensors are wires so that...
    i) If there is no fire, there is an open circuit
    ii) When there's a fire, the input is pulled to ground
*/
{
if (digitalRead(bFireSeenPin)==LOW)
            return true; else return false;
}

//****************************
boolean csBurglar(){
if (digitalRead(bBurgSeenPin)==HIGH)
            return true; else return false;
}

//****************************
void FireBellOn(boolean boOn){
/*This subroutine changes the state of the
  FireBell output, depending on what is
  passed to boOn. The code should be
  tweaked so that whatever your hardware,
  FireBellOn(true) gives rise to a ringing
  bell. Note that the bell should continue
  to ring until a FireBellOn(false) call
  is executed.*/
if (boOn) {digitalWrite(bFireBellPin,HIGH);
           boFireBellOn=true;//added in patch of 15Oct11
          }
      else{digitalWrite(bFireBellPin,LOW);}
}

//****************************
void BurglarBellOn(boolean boOn){
//See notes under FireBellOn(), above.
if (boOn) {digitalWrite(bBurglarBellPin,HIGH);
           boBurglarBellOn=true;//added in patch of 15Oct11
          }
      else{digitalWrite(bBurglarBellPin,LOW);}
}

//****************************
void BeepOn(boolean boOn){
/*The "beeper" is a quiet sound generating
  device, for sending prompts to users
  during everyday interaction with the system.
  The comments made in the FireBellOn
  subroutine, above, also apply to BeepOn()*/

if (boOn) {digitalWrite(bBeepPin,HIGH);}
      else{digitalWrite(bBeepPin,LOW);};
}

//****************************
void ArduSimpSrv(){
  /*This for future developments... as long as...
   a)   What goes here doesn't block the
     execution of loop() for longer than a
     "burglar seen" or "fire seen" or a user
     will be willing to wait for a response,
   and
   b)   What goes here won't mind waiting
     through the delays in Block"B, 2C and 3A

   ... you can add what you like!

   I suspect that I will be able to import
   all of the code of my ArduSimpSrv to here,
   creating a burglar alarm panel which can
   be accessed across the internet.

   (More on ArduSimpSrv at ArduServer.com)*/
}

//****************************
void XmitArming(){
//For debugging work, you could add something like...
//Serial.println("Sending Arming message");
//to your code here, to get programmer-friendly
//feedback as to where the program's got to.
//The message would be available in the serial
//monitor, which you should read up on...
//  http://sheepdogguides.com/arduino/aht1serimoni.htm
//... if you are not already using it! (You
//need to do some initialization in setup()
//to use the serial monitor... but it is easy.)
for (int8_t bTmp=0; bTmp<8; bTmp++)
{BeepOn(true);
 delay(600);
 BeepOn(false);
 delay(80);};
for (int8_t bTmp=0; bTmp<4; bTmp++)
{BeepOn(true);
 delay(40);
 BeepOn(false);
 delay(60);};
}

//****************************
void XmitHaveDisarmed(){
for (int8_t bTmp=0; bTmp<6; bTmp++)
{BeepOn(true);
 delay(8);
 BeepOn(false);
 delay(70);};
}

//****************************
void XmitMorseS(){
for (int8_t bTmp=0; bTmp<3; bTmp++)
  {BeepOn(true);
   delay(bDit);
   BeepOn(false);
   delay(bPostBit);
   };
 delay(bPostLett);
}
//****************************
void XmitMorseOh(){
for (int8_t bTmp=0; bTmp<3; bTmp++)
  {BeepOn(true);
   delay(bDah);
   BeepOn(false);
   delay(bPostBit);
   };
 delay(bPostLett);
}

//****************************
void XmitAboutToStartBell(){
 //first send "SOS" in Morse a few times...
for (int8_t bTmp=0; bTmp<3; bTmp++)
   {XmitMorseS();
   delay(bPostLett);
   XmitMorseOh();
   delay(bPostLett);
   XmitMorseS();
   delay(bPostLett);
   delay(bPostWord);
   };
 //Then an urgent beep, beep, beep...
for (int8_t bTmp=0; bTmp<12; bTmp++)
   {BeepOn(true);
   delay(100);
   BeepOn(false);
   delay(300);
   };
}

//****************************
void WaitFor(int16_t bHowLong){
/*The reason for using this, rather than just
putting "delay(x)" in at each place there
is a "WaitFor" is to allow the programmer
to switch the speed of various timeouts,
e.g. the pause after the "beeps" during
the system arming process. During debugging,
the parameter of the "delay" below can be
low, say 1. When development is finishing,
using 100 will make the delay specified in
any WaitFor calls be in tenths of seconds,
e.g. WaitFor(10) would cause a one second
pause... during which the program is blocked,
by the way.*/
delay(10*bHowLong);
}

//****************************
void Block1A(){
  //See diagram at
  //  Q-EDIT: PROVIDE LINK... somewhere in
  //http://sheepdogguides.com/arttoc.htm, probably
 if(csWantArmed()) Block2B(); else Block2A();
 }

//****************************
void Block1B(){
 if(csWantArmed()) Block2D(); else Block2C();
  }

//****************************
void Block2A(){
  if(csFireSeen()){
     if (boFireBellOn==false)//2nd if added in patch of 15Oct11
       {FireBellOn(true);};}
  }

//****************************
void Block2B(){
  //Exit delay consists of time it takes to
  //   execute "XmitArming" + time taken with "WaitFor"
  XmitArming();
  WaitFor(1);
  boPrevArmed=true;
  }


//****************************
void Block2C(){
  boPrevArmed=false;
  XmitHaveDisarmed();
  }

//****************************
void Block2D(){
 if(csBurglar()){Block3A();}else{Block2A();}
 }

//****************************
void Block3A(){
 //XmitAboutToStartBell+WaitFor() define how
 //  much warning householders get that
 //  they've tripped the alarm, and that it
 //  will shortly begin ringing the bell,
 //  unless steps are taken to prevent that.
 //Make it too long, and your burglar will
 //  guess what's happening, and find a way
 //  to disable the system.

 //Initially, was just the three lines with "//original".
 //They were "wrapped" in if (boBurglarBell==false)...
 //   as part of patch of 15Oct11

if (boBurglarBellOn==false){
     XmitAboutToStartBell();//original
     WaitFor(1);//origianl
     BurglarBellOn(true);//original
     };
}//end of Block 3A

//****************************
void setup(){
/*Configure inputs... No pinMode stmt needed... Arduino pins
   default to "set for input"...
   but we may want to "connect" the internal pull up
   resistors, depending on how you wired the sensors
   to the Arduino.*/

//Whether you want all, some or none of the next three
//   lines depends on how your sensors are wired.
digitalWrite(bArmedWantedPin,HIGH);
digitalWrite(bBurgSeenPin,HIGH);
digitalWrite(bFireSeenPin,HIGH);

/*Configure outputs... And establish initial states...
   beware: Your any of the outputs may be briefly high
   during system start up. You external circuits and
   circumstances must be such that this doesn't matter
   (Note that in general, digtal pin 13 usually becomes
   an output and pulses high during any system reset.
   I don't think it REMAINS an output, unless a
   pinMode statement accomplishes this.*/
pinMode(bBeepPin,OUTPUT);
pinMode(bFireBellPin,OUTPUT);
pinMode(bBurglarBellPin,OUTPUT);
/*Set initial states...

NOTE THIS about the various "On" subroutines. We'll
use the BurglarBellOn() subroutine as an example.
That subroutine is NOT only for turning the bell
on... it is also for turning it OFF. The parameter
passed with the call makes all the difference.*/
BeepOn(false);
BurglarBellOn(false);
FireBellOn(false);

delay(10);/*(Will be more than 10 in final edition.
 Wait a bit, so that sensors can "warm up", and start
   returning stable readings.
 Beeper should probably beep on and
   off while delay is executed. Not hard to add.
*/

XmitHaveDisarmed();//debug

boPrevArmed=csWantArmed();
}

//++++++++++++++++++++++++++++
/* Now we come to loop() and xloop()
   One of them is the finished code, needed to implement
      the alarm system
   The other is code which lets you quickly check the
      wiring and low level subroutines for your
      inputs and outputs.

   JUST BY MOVING THE "x", you can disable one of the two
      versions of the "loop()" subroutine. The other, the
      one for the moment called "xloop()" does no harm
      "sitting there", just in case you need its code again.
   */

//****************************
void loop(){
  //Standard Arduino main loop... one of two alternatives...
  //  .... the "finished alarm system code"...

  ArduSimpSrv();

  if (boPrevArmed) {Block1B();} else {Block1A();}
  }

void xloop(){
  //Standard Arduino main loop... a version for making tests...
  if (csBurglar()) BurglarBellOn(true); else BurglarBellOn(false);
  if (csFireSeen()) FireBellOn(true); else FireBellOn(false);
  if (csWantArmed()) BeepOn(true); else BeepOn(false);
}
