// this code assumes that pull-down resistors are used - invert the state if using pull-ups.

int pins[] = {2,3,5,7,8,9}; // array holding the pins connected to switches
int number_of_pins = sizeof(pins)/ sizeof(pins[0]);  // determines how many pins in the above array

int switchStates = 0; // this will hold a bitmask of switch states

void setup()
{
 Serial.begin(9600);
}

void loop()
{
   // save the switch states as bits in the variable switchStates
   for(int i=0; i < number_of_pins; i++)
   {
     // set bits in switchState to represent the state of the switches
     int state = digitalRead( pins[i]);
     bitWrite(switchStates, i, state);
   }

   switch (switchStates)
   {
      // test for some combinations of switches we care about
     case  B000001: Serial.println("the first switch (pin 2) is pressed"); break;
     case  B000101: Serial.println("switches on pins 2 and 5 are pressed"); break;
     case  B111111: Serial.println("all switches are pressed"); break;
   }
   showFlags();
   delay(1000);
}

// reports flags that are set to see how the switchStates variable is used
void showFlags()
{
   Serial.println(switchStates, BIN);
   for(int sw = 0; sw < number_of_pins; sw++)
   {
     Serial.print("Switch ");
     Serial.print(sw);
     if( bitRead(switchStates, sw) == true)
        Serial.println(" is pressed ");
     else
        Serial.println(" is not pressed");
   }
   Serial.println();
}
