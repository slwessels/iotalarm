//http://www.bristolwatch.com/arduino/arduino_irq.htm
//

#define LED1 0
#define LED2 2
#define SW1 4
#define SW2 15
#define SW3 13
#define SW4 12
#define SW5 14
#define SW6 5
volatile byte alarm_active = LOW;
volatile byte test_mode = LOW;
volatile int triggered = 0;

void toggle(byte pinNum) {
  byte pinState = !digitalRead(pinNum);
  digitalWrite(pinNum, pinState);
}

void setup()  {
  Serial.begin(115200);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  digitalWrite(LED1, 0); // LED off
  digitalWrite(LED2, 0); // LED off
  pinMode(SW1, INPUT);
  pinMode(SW2, INPUT);
  pinMode(SW3, INPUT);
  pinMode(SW4, INPUT);
  pinMode(SW5, INPUT);
  pinMode(SW6, INPUT);
  attachInterrupt(digitalPinToInterrupt(SW1), ISR1, CHANGE);
  attachInterrupt(digitalPinToInterrupt(SW2), ISR2, CHANGE);
  attachInterrupt(digitalPinToInterrupt(SW3), ISR3, CHANGE);
  attachInterrupt(digitalPinToInterrupt(SW4), ISR4, CHANGE);
  attachInterrupt(digitalPinToInterrupt(SW5), ISR5, CHANGE);
  attachInterrupt(digitalPinToInterrupt(SW6), ISR6, CHANGE);
  // interrupt 0 digital pin 0 connected SW0
  //attachInterrupt(SW2, ISR1, CHANGE);
  // interrupt 1 digital pin 5 connected SW1
}

void loop() {
  // do nothing

  if (triggered != 0)
  {
    Serial.println ("ISR trggered = " + String(triggered));
  }

  while (triggered >= 1)
  {
    Alarm(LED2);
    delay(100);
    Serial.println ("Done");
  }
  delay(250);
}

// can't use delay(x) in IRQ routine
void ISR1() {
  triggered = 4;
  toggle(LED1);
}

// can't use delay(x) in IRQ routine
void ISR2() {
  triggered = 15;
  toggle(LED1);
}

// can't use delay(x) in IRQ routine
void ISR3() {
  triggered = 13;
  toggle(LED1);
}

// can't use delay(x) in IRQ routine
void ISR4() {
  triggered = 12;
  toggle(LED1);
}

// can't use delay(x) in IRQ routine
void ISR5() {
  triggered = 14;
  toggle(LED1);
}

// can't use delay(x) in IRQ routine
void ISR6() {
  triggered = 5;
  toggle(LED1);
}


void Alarm(int led)
{
  while (triggered > 0)
  {
    digitalWrite(led,HIGH);
    delay(100);
    digitalWrite(led, LOW);
    delay(100);
    triggered--;
  }
}
