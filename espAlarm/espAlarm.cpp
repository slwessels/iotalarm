/*
  espAlarm.cpp - Library for creating an espAlarm.
  Created by Stephan Wessels
*/
#include <espAlarm.h>

//=======================================================================
//Variable definitions
//=======================================================================
unsigned long timestamp = 0;
enum stateEnum {notArmed, inProgress, armed, triggered, paused};
stateEnum alarmState;
stateEnum prevAlarmState;
int inputTriggers[] = {4};//{4,5,14,13,12,15};
#define triggerLen (sizeof(inputTriggers)/sizeof(int *)) //array size
int readTriggers[triggerLen];//array to read input data into
bool triggerInterrupt = false;

int inputChannels[] = {13, 12, 14, 10, 5}; //{4,5,14,13,12,15};
//Had to swap out 15 to be output (due to hardware design - LOW at startup)
#define channelLen (sizeof(inputChannels)/sizeof(int *)) //array size
bool channelInterrupt = false;
int readChannels[channelLen];//array to read input data into

int outputAlerts[] = {15, 2, 0};
//Had to swap out 0 & 2 as inputs due to hardware design - HIGH at startup
#define outputLen (sizeof(outputAlerts)/sizeof(int *)) //array size

// stateEnum zone[3][1] = {{0,1,2},{notArmed}};

//=======================================================================
//Global Functions - mostly due to callbacks and hardware layer
//=======================================================================
void setupChannel(int input)
{
  attachInterrupt(digitalPinToInterrupt(input), isrChannel, CHANGE);
}

//-----------------------------------------------------------------------
void setupTrigger(int input)
{
  attachInterrupt(digitalPinToInterrupt(input), isrTrigger, CHANGE);
}

//-----------------------------------------------------------------------
void isrChannel()
{
  //Prepare the program to read new states
  channelInterrupt = true;
}

//-----------------------------------------------------------------------
void isrTrigger()
{
  //Prepare the program to read new states
  triggerInterrupt = true;
}

//=======================================================================
//Class Functions
//=======================================================================
EspAlarm::EspAlarm(void (*pCallbackFunction)(String))
{
  pCallback = pCallbackFunction;
}

//-----------------------------------------------------------------------
EspAlarm::~EspAlarm()
{
  /*nothing to destruct*/
}

//-----------------------------------------------------------------------
void EspAlarm::setup()
{
  alarmState = notArmed;
  prevAlarmState = notArmed;
  //Setup as input and interrupts for trigger inputs

  setupInterrupts(inputTriggers, triggerLen, false);
  //Setup as input and interrupts for channel inputs
  setupInterrupts(inputChannels, channelLen, true);

  //Setup outputs - the old fasioned way
  for (int i = 0; i < outputLen; i++)
  {
    pinMode(outputAlerts[i], OUTPUT);
    digitalWrite(outputAlerts[i], LOW);//set to LOW
  }
}

//-----------------------------------------------------------------------
void EspAlarm::setupInterrupts(int inputList[], int length, bool channel)
{
  //wait for everything to start up - insure comms to MQTT
  // delay(3000);
  // String msg = "";
  //Setup inputs interrupts
  for (int i = 0; i < length; i++)
  {
    //First setup pin mode
    pinMode(inputList[i], INPUT); //was INPUT, but now set as INPUT_PULLUP
    //Then attachInterrupt based on type
    if (channel)
    {
      setupChannel(digitalPinToInterrupt(inputList[i]));
      // attachInterrupt(digitalPinToInterrupt(inputList[i]), isrChannel, CHANGE);
      externalComms(" isrChannel as Change " + inputList[i]);
      // msg += " isrChannel as Change " + String(inputList[i])+"\n";
    }
    else
    {
      setupTrigger(digitalPinToInterrupt(inputList[i]));
      // attachInterrupt(digitalPinToInterrupt(inputList[i]), isrTrigger, CHANGE);
      externalComms(" isrTrigger as Change " + String(inputList[i]));
      // msg += " isrTrigger as Change " + String(inputList[i])+"\n";
    }
  }
  // return msg;
}

//-----------------------------------------------------------------------
void EspAlarm::readInputs(int inputList[], int outputList[], int inputLen)
{
  String msg = "";
  // externalComms("evaluate function");
  for (int i = 0; i < inputLen; i++)
  {
    outputList[i] = digitalRead(inputList[i]);
    msg = msg + " | " + String(outputList[i]);
  }
  externalComms(msg);
  // return msg;
}

//-----------------------------------------------------------------------
void EspAlarm::setDisArmed(bool state)
{
  // externalComms("Triggered setArmed Function!");
  if (state == true)
  {
    //Set alarmState to new state
    alarmState = notArmed;
    digitalWrite(outputAlerts[0], HIGH);//Show armed light
    digitalWrite(outputAlerts[1], LOW);//Switch off ALARM TRIGGERED Light
    externalComms(getAlarmState() +": Alarm disarmed");
  }
  else
  {
    //Set alarmState to new state
    alarmState = armed;
    digitalWrite(outputAlerts[0], LOW);//Switch off armed light
    externalComms(getAlarmState() +": Alarm ARMED!");
  }
}

//-----------------------------------------------------------------------
void EspAlarm::alarmTrigger(bool isArmed)
{
  externalComms(getAlarmState() +": Alarm triggered!");
  alarmState = triggered;
  //Only sound alarm if armed
  if (isArmed)
  {
    digitalWrite(outputAlerts[1], HIGH);//set to HIGH as alarm got triggered
    externalComms("ALARM-ALARM-ALARM-ALARM-ALARM-ALARM-ALARM-ALARM-ALARM");
  }
}

//-----------------------------------------------------------------------
void EspAlarm::evaluate()
{
  if (triggerInterrupt)
  {
    readInputs(inputTriggers, readTriggers, triggerLen);
    //Don't unset as we still need to deal with the actual switching
  }

  if (channelInterrupt)
  {
    readInputs(inputChannels, readChannels, channelLen);
  }
}

//-----------------------------------------------------------------------
void EspAlarm::influence()
{
  //Set the alarm if triggerInterrupt set
  if (triggerInterrupt)
  {
    readTriggers[0] == LOW ? setDisArmed(true) : setDisArmed(false);
    //Interrupt actioned, resetting flag
    triggerInterrupt = false;

    String msg = "";
    msg = "Trigger Interrupt serviced " + String(readTriggers[0]) + " - " + getAlarmState();
    externalComms(msg);
  }

  if (channelInterrupt)
  {
    //alarm triggering to be implemented here!
    alarmState == armed ? alarmTrigger(true) : alarmTrigger(false);
    // if (alarmState == armed)
    // {  digitalWrite(outputAlerts[0], HIGH);//set to LOW

    //   //Alarm triggered - Must be noted
    //   //externalComms("ALARM-ALARM-ALARM-ALARM-ALARM-ALARM-ALARM-ALARM-ALARM");
    //   // alarmTrigger(true);
    // }
    //Interrupt actioned, resetting flag
    channelInterrupt = false;

  }
}
//-----------------------------------------------------------------------
String EspAlarm::getAlarmState()
{
  switch (alarmState)
  {
    case notArmed: return "notArmed";
    case inProgress: return "inProgress";
    case armed: return "armed";
    case triggered: return "triggered";
    case paused: return "paused";
    default: return "UnKnownState";
  }
}

//-----------------------------------------------------------------------
void EspAlarm::run()
{
  evaluate();
  influence();
}
//=======================================================================
