/*
  espAlarm.h - Library for creating an espAlarm.
  Created by Stephan Wessels
*/

#ifndef EspAlarm_h
#define EspAlarm_h

#include <Arduino.h>
// #include <ESP8266WiFi.h>

void setupChannel(int input);
void setupTrigger(int input);
void isrChannel();
void isrTrigger();
void externalComms(String message);

class EspAlarm
{
  public:
    EspAlarm(void (*pCallbackFunction)(String));
    ~EspAlarm();
    void setup();
    void setupInterrupts(int inputList[], int length, bool channel);
    void run();
    void setDisArmed(bool state);
    String getAlarmState();

  private:
    void (*pCallback)(String);
    void readInputs(int inputList[], int outputList[], int inputLen);
    void alarmTrigger(bool isArmed);
    void evaluate();
    void influence();
};

#endif
